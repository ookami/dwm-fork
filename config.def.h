/* See LICENSE file for copyright and license details. */

#define SESSION_FILE "/tmp/dwm-session"

/* appearance */
static unsigned int borderpx  = 1;        /* border pixel of windows */
static unsigned int snap      = 32;       /* snap pixel */
static unsigned int gappih    = 20;       /* horiz inner gap between windows */
static unsigned int gappiv    = 10;       /* vert inner gap between windows */
static unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static int smartgaps          = 1;        /* 1 means no outer gap when there is only one window */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static char font[]            = "monospace:size=10";
static char font2[]           = "monospace:size=10";
static const char *fonts[]    = { font, font2 };
static char dmenufont[]       = "monospace:size=10";
static char normbgcolor[]     = "#222222";
static char normbordercolor[] = "#444444";
static char normfgcolor[]     = "#bbbbbb";
static char selfgcolor[]      = "#eeeeee";
static char selbordercolor[]  = "#005577";
static char selbgcolor[]      = "#005577";
static char statusfgcolor[]   = "#005577";
static char statusbgcolor[]   = "#222222";
static char hidfgcolor[]      = "#005577";
static char hidbgcolor[]      = "#000000";
static char hidbordercolor[]  = "#444444";
static char *colors[][3]      = {
	/*                fg             bg             border   */
	[SchemeNorm]  = { normfgcolor,   normbgcolor,   normbordercolor },
	[SchemeSel]   = { selfgcolor,    selbgcolor,    selbordercolor  },
	[SchemeStatus]= { statusfgcolor, statusbgcolor, NULL            },
	[SchemeHid]   = { hidfgcolor,    hidbgcolor,    hidbordercolor  },
};
#define ICONSIZE 16 /* icon size */
#define ICONSPACING 5 /* space between icon and title */


/* status bar */
static const Block blocks[] = {
	/* fg            command                        interval        signal */
	{ "#00fe00",     "sb-clock",                    1,              1},
//	{ statusfgcolor, "sb-disk",                     9000,           2},
	{ "#eeeeee",     "sb-battery",                  10,             3},
	{ "#ffa74e",     "sb-cpubars",                  1,              14},
	{ "#ba45ae",     "sb-memory",                   1,              15},
	{ "#fe0000",     "sb-internet",                 10,             4},
//	{ statusfgcolor, "sb-mailbox",                  0,              5},
//	{ "#000001",     "sb-moonphase",                0,              6},
//	{ statusfgcolor, "sb-forecast",                 0,              7},
	{ "#0000fe",     "sb-volume",                   0,              8},
//	{ "#F77000",     "sb-pacpackages",              0,              9},
//	{ "#177000",     "sb-sync",                     0,              10},
//	{ statusfgcolor, "sb-music",                    1,              11},
//	{ statusfgcolor, "sb-tasks",                    10,             12},
//	{ statusfgcolor, "sb-notes",                    0,              13},
};

/* inverse the order of the blocks, comment to disable */
#define INVERSED 1
/* delimeter between blocks commands. NULL character ('\0') means no delimeter. */
static char delimiter[] = " ";
/* max number of character that one block command can output */
#define CMDLENGTH 63


/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6"};//, "7", "8", "9" };

static char fg1[] = "#ffffff";
static char bg1[] = "#ff0000";
static char fg2[] = "#ffffff";
static char bg2[] = "#ff7f00";
static char fg3[] = "#000000";
static char bg3[] = "#ffff00";
static char fg4[] = "#000000";
static char bg4[] = "#00ff00";
static char fg5[] = "#ffffff";
static char bg5[] = "#0000ff";
static char fg6[] = "#ffffff";
static char bg6[] = "#4b0082";
//static char fg7[] = "#ffffff";
//static char bg7[] = "#9400d3";
//static char fg8[] = "#000000";
//static char bg8[] = "#ffffff";
//static char fg9[] = "#ffffff";
//static char bg9[] = "#000000";
static const char *tagsel[][2] = {
	{ fg1, bg1 },
	{ fg2, bg2 },
	{ fg3, bg3 },
	{ fg4, bg4 },
	{ fg5, bg5 },
	{ fg6, bg6 },
//	{ fg7, bg7 },
//	{ fg8, bg8 },
//	{ fg9, bg9 },
};

static const Rule rules[] = {
	/* xprop(1):
	*	WM_CLASS(STRING) = instance, class
	*	WM_NAME(STRING) = title
	*/
	/* class      instance    title       tags mask     isfloating   ishidden   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           0,         -1 },
	{ NULL, "Godot_Engine",   NULL,       0,            1,           0,         -1 },
	{ NULL, "Godot_Editor",   NULL,       0,            0,           0,         -1 },
	{ NULL,NULL,"100% Orange Juice Settings",0,        1,           0,         -1 },
	{ "matplotlib",NULL,      NULL,       0,            1,           0,         -1 },
};
/* window swallowing */
static const int swaldecay = 3;
static const int swalretroactive = 1;
static const char swalsymbol[] = "👅";

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define HOLDKEY 0xffeb // 0 - disable; 0xffe9 - Mod1Mask; 0xffeb - Mod4Mask

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *dmenuopen[]    = { "dmenu_open", NULL };
static const char *pswdcmd[]      = { "spm", "dmenu", NULL };
static const char *onf[]          = { "onf", NULL };
static const char *mpc_open[]     = { "mpc-open-dir", NULL };
static const char *ncmpcpp[]      = { "st", "-e", "ncmpcpp", "-q", NULL };
static const char *pasteprimary[] = { "paste_primary", NULL };
static const char *mpdnotify[]    = { "mpd-notification-show", NULL };
static const char *brightup[]     = { "lights", "5", NULL };
static const char *brightdown[]   = { "lights", "-5", NULL };
static const char *togglemute[]   = { "togglemute", NULL };
static const char *screenshot[]   = { "scrotclip", NULL };
//static const char *bookmarks[]    = { "bookmarks", NULL };
//static const char *addbm[]        = { "bookmarks", "add", NULL };
//static const char *emojis[]       = { "emojis", NULL };
//static const char *addemoji[]     = { "emojis", "add", NULL };
static const char *nextmusic[]    = { "mpc", "next", NULL };
static const char *prevmusic[]    = { "mpc", "prev", NULL };
static const char *raisevol[]     = { "mpc-volume", "+2", NULL };
static const char *lowervol[]     = { "mpc-volume", "-2", NULL };
static const char *togglemusic[]  = { "mpc", "toggle", NULL };
static const char *stopmusic[]    = { "mpc", "stop", NULL };
static const char *seekplus[]     = { "mpc", "seekthrough", "+5", NULL };
static const char *seekminus[]    = { "mpc", "seekthrough", "-5", NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "borderpx",           INTEGER, &borderpx },
		{ "snap",               INTEGER, &snap },
		{ "gappih",             INTEGER, &gappih },
		{ "gappiv",             INTEGER, &gappiv },
		{ "gappoh",             INTEGER, &gappoh },
		{ "gappov",             INTEGER, &gappov },
		{ "smartgaps",          INTEGER, &smartgaps },
		{ "showbar",            INTEGER, &showbar },
		{ "topbar",             INTEGER, &topbar },
		{ "font",               STRING,  &font },
		{ "font2",              STRING,  &font2 },
		{ "dmenufont",          STRING,  &dmenufont },
		{ "normbgcolor",        STRING,  &normbgcolor },
		{ "normbordercolor",    STRING,  &normbordercolor },
		{ "normfgcolor",        STRING,  &normfgcolor },
		{ "selbgcolor",         STRING,  &selbgcolor },
		{ "selbordercolor",     STRING,  &selbordercolor },
		{ "selfgcolor",         STRING,  &selfgcolor },
		{ "statusfgcolor",      STRING,  &statusfgcolor },
		{ "statusbgcolor",      STRING,  &statusbgcolor },
		{ "hidfgcolor",         STRING,  &hidfgcolor },
		{ "hidbgcolor",         STRING,  &hidbgcolor },
		{ "hidbordercolor",     STRING,  &hidbordercolor },
		{ "fg1",                STRING,  &fg1 },
		{ "fg2",                STRING,  &fg2 },
		{ "fg3",                STRING,  &fg3 },
		{ "fg4",                STRING,  &fg4 },
		{ "fg5",                STRING,  &fg5 },
		{ "fg6",                STRING,  &fg6 },
//		{ "fg7",                STRING,  &fg7 },
//		{ "fg8",                STRING,  &fg8 },
//		{ "fg9",                STRING,  &fg9 },
		{ "bg1",                STRING,  &bg1 },
		{ "bg2",                STRING,  &bg2 },
		{ "bg3",                STRING,  &bg3 },
		{ "bg4",                STRING,  &bg4 },
		{ "bg5",                STRING,  &bg5 },
		{ "bg6",                STRING,  &bg6 },
//		{ "bg7",                STRING,  &bg7 },
//		{ "bg8",                STRING,  &bg8 },
//		{ "bg9",                STRING,  &bg9 },
		{ "nmaster",            INTEGER, &nmaster },
		{ "resizehints",        INTEGER, &resizehints },
		{ "mfact",              FLOAT,   &mfact },
};

#include "movestack.c"
#include "exitdwm.c"
static const Key keys[] = {
	/* key definitions = /usr/include/X11/keysymdef.h */
	/* modifier                     key        function        argument */
	// spawn commands
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_w,      spawn,          {.v = dmenuopen} },
	{ MODKEY,                       XK_p,      spawn,          {.v = pswdcmd } },
	{ MODKEY,                       XK_n,      spawn,          {.v = onf } },
	{ MODKEY,                       XK_Insert, spawn,          {.v = pasteprimary } },
	{ MODKEY|ControlMask,           XK_Left,   spawn,          {.v = brightdown } },
	{ MODKEY|ControlMask,           XK_Right,  spawn,          {.v = brightup } },
	{ MODKEY,                       XK_z,      spawn,          {.v = togglemute } },
	{ 0,                            XK_Print,  spawn,          {.v = screenshot } },
	{ MODKEY,                       XK_F1,     spawn,          {.v = togglemusic } },
	{ MODKEY,                       XK_F2,     spawn,          {.v = stopmusic } },
	{ MODKEY,                       XK_F3,     spawn,          {.v = prevmusic } },
	{ MODKEY,                       XK_F4,     spawn,          {.v = nextmusic } },
	{ MODKEY,                       XK_F6,     spawn,          {.v = lowervol } },
	{ MODKEY,                       XK_F7,     spawn,          {.v = raisevol } },
	{ MODKEY,                       XK_F8,     spawn,          {.v = mpdnotify } },
	{ MODKEY|ShiftMask,             XK_F8,     spawn,          {.v = mpc_open } },
	{ MODKEY,                       XK_F9,     spawn,          {.v = seekminus } },
	{ MODKEY,                       XK_F10,    spawn,          {.v = seekplus } },
	{ MODKEY|ShiftMask,             XK_m,      spawn,          {.v = ncmpcpp } },
	// toggle bar
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ 0,                            HOLDKEY,   holdbar,        {0} },
	// change active window
	{ MODKEY,                       XK_j,      focusstackvis,  {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstackvis,  {.i = -1 } },
	// change active window including hidden windows
	{ MODKEY|ShiftMask,             XK_j,      focusstackhid,  {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      focusstackhid,  {.i = -1 } },
	// add or remove to master stack
	{ MODKEY|ControlMask,           XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ControlMask,           XK_d,      incnmaster,     {.i = -1 } },
	// increase or decrease master stack width
	{ MODKEY|ControlMask,           XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY|ControlMask,           XK_l,      setmfact,       {.f = +0.05} },
	// increase or decrease client factor on stack
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = -0.25} },
	// reset client factor
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	// move client through stack
	{ MODKEY|ControlMask,           XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ControlMask,           XK_k,      movestack,      {.i = -1 } },
	// focus active client
	{ MODKEY,                       XK_v,      zoom,           {0} },
	// increase or decrease gaps
	{ MODKEY|Mod1Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	// reset gaps
	{ MODKEY|Mod1Mask,              XK_0,      togglegaps,     {0} },
	// toggle gaps
	{ MODKEY|Mod1Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	// change to previous tag
	{ MODKEY,                       XK_Tab,    view,           {0} },
	// close client
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	// change to tile layout
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	// change to monocle layout
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[1]} },
	// change to deck layout
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[4]} },
	// change to previous layout
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	// toggle floating window
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	// toggle alwaysontop window
	{ MODKEY|ControlMask,           XK_space,  togglealwaysontop, {0} },
	// toggle fullscreen
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	// view every window from every tag
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	// set active window to every tag
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	// multiple monitors bindings
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	// change to adjacent tag
	{ MODKEY,                       XK_Right,  viewnext,       {0} },
	{ MODKEY,                       XK_Left,   viewprev,       {0} },
	// send client to next tag
	{ MODKEY|ShiftMask,             XK_Right,  tagtonext,      {0} },
	{ MODKEY|ShiftMask,             XK_Left,   tagtoprev,      {0} },
	// increase or decrease border pixel
	{ MODKEY|ControlMask,           XK_minus,  setborderpx,    {.i = -1 } },
	{ MODKEY|ControlMask,           XK_equal,  setborderpx,    {.i = +1 } },
	// reset border pixel
	{ MODKEY|ControlMask,           XK_BackSpace, setborderpx,    {.i = 0 } },
	// unhide active hidden client
	{ MODKEY,                       XK_s,      show,           {0} },
	// unhide all hidden clients
	{ MODKEY|ShiftMask,             XK_s,      showall,        {0} },
	// hide active client
	{ MODKEY,                       XK_h,      hide,           {0} },
	// move floating window to center
	{ MODKEY,                       XK_x,      movecenter,     {0} },
	// cycle layouts
	{ MODKEY,                       XK_Up,     layoutscroll,   {.i = -1 } },
	{ MODKEY,                       XK_Down,   layoutscroll,   {.i = +1 } },
	// unswallow clients
	{ MODKEY,                       XK_u,      swalstopsel,    {0} },
	// change to tag
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
//	TAGKEYS(                        XK_7,                      6)
//	TAGKEYS(                        XK_8,                      7)
//	TAGKEYS(                        XK_9,                      8)
	// open exitmenu
	{ MODKEY|ShiftMask,             XK_q,      exitdwm,        {0} },
	// restart dwm
	{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkLtSymbol,          MODKEY,         Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          MODKEY,         Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button1,        togglewin,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkWinTitle,          MODKEY,         Button1,        togglewin,      {0} },
	{ ClkWinTitle,          MODKEY,         Button2,        zoom,           {0} },

	{ ClkStatusText,        0,              Button1,        sendstatusbar,   {.i = 1 } },
	{ ClkStatusText,        0,              Button2,        sendstatusbar,   {.i = 2 } },
	{ ClkStatusText,        0,              Button3,        sendstatusbar,   {.i = 3 } },
	{ ClkStatusText,        0,              Button4,        sendstatusbar,   {.i = 4 } },
	{ ClkStatusText,        0,              Button5,        sendstatusbar,   {.i = 5 } },
	{ ClkStatusText,        ShiftMask,      Button1,        sendstatusbar,   {.i = 6 } },
	{ ClkStatusText,        MODKEY,         Button1,        sendstatusbar,   {.i = 1 } },
	{ ClkStatusText,        MODKEY,         Button2,        sendstatusbar,   {.i = 2 } },
	{ ClkStatusText,        MODKEY,         Button3,        sendstatusbar,   {.i = 3 } },
	{ ClkStatusText,        MODKEY,         Button4,        sendstatusbar,   {.i = 4 } },
	{ ClkStatusText,        MODKEY,         Button5,        sendstatusbar,   {.i = 5 } },
	{ ClkStatusText,        MODKEY|ShiftMask,Button1,       sendstatusbar,   {.i = 6 } },

	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkClientWin,         MODKEY|ShiftMask, Button1,      swalmouse,      {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        view,           {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggleview,     {0} },
};

